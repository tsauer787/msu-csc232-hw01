/**
 * CSC232 - Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file   Bag.h
 * @authors Frank M. Carrano
 *          Timothy M. Henry
 * @brief  Bag interface specification.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef HW01_BAG_H
#define HW01_BAG_H

#include <vector>

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {

    /**
     * @brief Bag Interface introduced in Chapter 1 in Listing 1-1 with
     * modifications by Jim Daehn.
     * @tparam T class template parameter; the type of element stored in this
     * `Bag`.
     */
    template <typename T>
    class Bag {
    public:
        /**
         * @brief Gets the current number of entries in this bag.
         * @return The integer number of entries currently in this `Bag` is
         * returned.
         */
        virtual int getCurrentSize() const = 0;

        /**
         * @brief Determine whether this `Bag` is empty.
         * @return  True if this `Bag` is empty; false otherwise.
         */
        virtual bool isEmpty() const = 0;

        /**
         * @brief Adds a new entry to this `Bag`.
         * @post If successful, `newEntry` is stored in the bag and the count
         * of items in the bag has increased by 1.
         * @param newEntry The object to be added as a new entry.
         * @return True if addition was successful, or false if not
         */
        virtual bool add(const T& newEntry) = 0;

        /**
         * @brief Removes one occurrence of a given entry from this `Bag` if
         * possible.
         * @post If successful, `anEntry` has been removed from the bag and
         * the count of items i the bag has decreased by 1.
         * @param anEntry The entry to be removed.
         * @return True if removal was successful, or false if not.
         */
        virtual bool remove(const T& anEntry) = 0;

        /**
         * @brief Removes all entries from this bag.
         * @post Bag contains no items, and the count of items is 0.
         */
        virtual void clear() = 0;

        /**
         * @brief Counts the number of times a given entry appears in this bag.
         * @param anEntry The entry to be counted.
         * @return The number of times `anEntry` appears in the bag.
         */
        virtual int getFrequencyOf(const T& anEntry) const = 0;

        /**
         * @brief Tests whether this bag contains a given entry.
         * @param anEntry The entry to locate.
         * @return True if bag contains `anEntry`, or false otherwise.
         */
        virtual bool contains(const T& anEntry) const = 0;

        /**
         * @brief Empties and then fills a given `std::vector` with all
         * entries that are in this `Bag`.
         * @return A `std::vector` containing copies of all the entries in
         * this `Bag`.
         */
        virtual std::vector<T> toVector() const = 0;

		/**
		 * @brief Creates a new Bag with the combined contents of this Bag and another 
		 * Bag supplied as the sole argument to this method.
		 * @param otherBag the other Bag used in constructing the union of bags.
		 * @return A Bag that contains all the elements of this bag and another
         * bag.
		 * @post The contents of this Bag and the otherBag remain unchanged.
		 */
		virtual Bag<T> unionWithBag(const Bag<T>& otherBag) const {
			// We need a new bag to return when we're all done...
			Bag<T> unionBag;
 
			// Get the elements of this Bag using an existing Bag operation
			std::vector myElements = this.toVector();
       
			// add the elements of this Bag into the unionBag
			for (T element : myElements) {
				unionBag.add(element);
			}
 
			// Get the elements of the otherBag using existing Bag operation
			std::vector otherElements = otherBag.toVector();
			for (T element : otherElements) {
				unionBag.add(element);
			}
 
			// Our new bag has everything we need, so return it to the caller
			return unionBag;
        }

		/**
		 * @brief Creates a new Bag containing only elements found in both this Bag and another 
		 * Bag supplied as the sole argument to this method.
		 * @param otherBag the other Bag used in constructing the intersection of bags
		 * @return A Bag that contains elements occurring in both this Bag and another Bag.
		 * @post The contents of this Bag and the otherBag remain unchanged.
		 */
		 virtual Bag<T> intersectWithBag(const Bag<T>& otherBag) const{
			//create bag to return after populating it
			Bag<T> intersectBag;
			
			// Get the elements of this Bag using an existing Bag operation
			std::vector myElements = this.toVector();
			
			//adds elements in both this Bag and otherBag
			for (T element : myElements) {
				if (otherBag.contains(element) && !(intersectBag.contains(element))){
					int lesserFrequency;
					//limits frequency of element entries added
					if (otherBag.getFrequencyOf(element) <= this.getFrequencyOf(element)){
						lesserFrequency = otherBag.getFrequencyOf(element);
						for(i=0; i < lesserFrequency; i++)
							intersectBag.add(element);
					}
					else{
						lesserFrequency = this.getFrequencyOf(element);
						for(i=0; i < lesserFrequency; i++)
							intersectBag.add(element);
					}	
				}
			}
			
			//returns new bag to caller
			return intersectBag;
		 }

		 /**
		 * @brief Creates a new Bag containing the entries that would be left in this Bag after 
		 * removing those that also occur in another Bag supplied as the sole argument to this method.
		 * @param otherBag the other Bag used in constructing the difference of bags
		 * @return A Bag that contains elements remaining in this Bag after removing those also in another Bag
		 * @post The contents of this Bag and the otherBag remain unchanged.
		 */
		 virtual Bag<T> differenceWithBag(const Bag<T>& otherBag) const{
			 //create bag to return after populating it
			Bag<T> differenceBag;
			
			// Get the elements of this Bag using an existing Bag operation
			std::vector myElements = this.toVector();
       
			// add the elements of this Bag into the differenceBag
			for (T element : myElements) {
			differenceBag.add(element);
			
			//gets elements of the other Bag
			std::vector otherElements = otherBag.toVector();
			
			//removes entries of elements also found in the other Bag
			for (T element : otherElements) {
				if (differenceBag.contains(element))
					differenceBag.remove(element);
			}
			
			//returns differenceBag to caller
			return differenceBag;
		 }

        /**
         * @brief Destroys this bag and frees its assigned memory.
         */
        virtual ~Bag() {
            // inlined, no-op
        }
    }; // end Bag
} // end namespace csc232

#endif //HW01_BAG_H
